## Patootie's Dot Files
### Dependencies
i3: `https://github.com/i3/i3`

13-gaps: `https://github.com/Airblader/i3`

Yabar: `https://github.com/geommer/yabar` [Not Using]

Polybar: `https://github.com/polybar/polybar`

Spicetify: `https://github.com/khanhas/spicetify-cli`

Fontawesome: `https://github.com/FortAwesome/Font-Awesome`

MellowPlayer: `https://github.com/ColinDuquesnoy/MellowPlayer` [Not using]

playerctl: `sudo apt-get install playerctl`

feh: `sudo apt-get install feh`

Consolas Font: `https://github.com/kakkoyun/linux.files/blob/master/fonts/Consolas.ttf`

Yosimite San Francisco Font: `https://github.com/supermarin/YosemiteSanFranciscoFont`

vim: `sudo apt-get install vim`

neofetch: `sudo apt-get install neofetch`

urxvt `sudo apt-get install rxvt-unicode`

mopidy `https://github.com/mopidy/mopidy`
mopidy-spotify `https://github.com/mopidy/mopidy-spotify`
mopidy-mpd `https://github.com/mopidy/mopidy-mpd``

ncmpcpp `sudo apt-get install ncmpcpp`

![alt text](https://i.imgur.com/rASTbIJ.jpg)
